FROM php:7.4
RUN apt-get update && apt-get install -y unzip wget git libssl-dev pkg-config
RUN pecl install mongodb 
RUN docker-php-ext-enable mongodb
WORKDIR /root
RUN wget https://getcomposer.org/installer
RUN php installer
RUN php composer.phar create-project jmikola/mongoqp
WORKDIR /root/mongoqp
# VOLUME /root/mongoqp/src/config.php
ADD config.php /root/mongoqp/src/config.php
RUN echo "date.timezone=America/Los_Angeles" > /usr/local/etc/php/conf.d/tz.ini
RUN echo "date.timezone=America/Los_Angeles" > /usr/local/etc/php/tz.ini
CMD php -S 0.0.0.0:8080 -t web
